package com.twuc.webApp.web;

import com.twuc.webApp.contract.Message;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class MazeControllerTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_400_given_illegal_argument_exception() {
        ResponseEntity<Message> entity = testRestTemplate.getForEntity("/mazes/type", Message.class);
        assertEquals(400, entity.getStatusCodeValue());
        assertEquals(APPLICATION_JSON, entity.getHeaders().getContentType());
        assertEquals("Invalid type: type", Objects.requireNonNull(entity.getBody()).getMessage());
    }
}
