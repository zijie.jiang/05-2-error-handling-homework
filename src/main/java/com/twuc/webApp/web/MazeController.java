package com.twuc.webApp.web;

import com.twuc.webApp.contract.Message;
import com.twuc.webApp.service.GameLevelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
public class MazeController {
    private static final Logger logger = LoggerFactory.getLogger(MazeController.class);
    private final GameLevelService gameLevelService;

    public MazeController(GameLevelService gameLevelService) {
        this.gameLevelService = gameLevelService;
    }

    @GetMapping("/mazes/{type}")
    @ResponseStatus(HttpStatus.OK)
    public void getMaze2(
            HttpServletResponse response,
            @RequestParam(required = false, defaultValue = "10") int width,
            @RequestParam(required = false, defaultValue = "10") int height,
            @PathVariable String type) throws IOException {

        response.setContentType(MediaType.IMAGE_PNG_VALUE);

        gameLevelService.renderMaze(
                response.getOutputStream(),
                width,
                height,
                type
        );
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Message> handleIllegalArgumentExp(IllegalArgumentException exp) {
        logger.error("[{}] ERROR {} - {}",
                Thread.currentThread().getName(),
                exp.getStackTrace()[0].getFileName(),
                exp.getMessage());
        return ResponseEntity.status(BAD_REQUEST)
                .contentType(APPLICATION_JSON)
                .body(new Message(exp.getMessage()));
    }
}
