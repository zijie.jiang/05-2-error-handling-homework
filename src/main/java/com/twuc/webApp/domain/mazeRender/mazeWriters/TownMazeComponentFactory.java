package com.twuc.webApp.domain.mazeRender.mazeWriters;

import com.twuc.webApp.domain.mazeRender.AreaRender;
import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.MazeComponentFactory;
import com.twuc.webApp.domain.mazeRender.MazeRenderSettings;
import com.twuc.webApp.domain.mazeRender.areaRenders.AreaColorRender;
import com.twuc.webApp.domain.mazeRender.cellRenders.DirectedImageCellRender;
import com.twuc.webApp.domain.mazeRender.cellRenders.RandomizedImageCellRender;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
public class TownMazeComponentFactory implements MazeComponentFactory {
    private static String getTownResourcePath(String path) { return "images/town/" + path; }

    @Override
    public List<AreaRender> createBackgroundRenders() {
        return Collections.singletonList(new AreaColorRender(Color.WHITE));
    }

    @Override
    public List<CellRender> createWallRenders() {
        CellRender render = new RandomizedImageCellRender(
            Arrays.asList(
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_1.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_2.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_3.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_4.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_5.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_6.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_7.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_8.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_9.png")),
                ResourceLoader.loadResource(getTownResourcePath("fake_3d_sm_wall_10.png"))
            ));
        return Collections.singletonList(render);
    }

    @Override
    public List<CellRender> createGroundRenders() {
        BufferedImage northSouth = ResourceLoader.loadResource(getTownResourcePath("road_north_south.png"));
        BufferedImage eastWest = ResourceLoader.loadResource(getTownResourcePath("road_east_west.png"));
        CellRender render = new DirectedImageCellRender(
            northSouth,
            northSouth,
            eastWest,
            eastWest,
            northSouth,
            ResourceLoader.loadResource(getTownResourcePath("road_north_east.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_north_west.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_south_east.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_south_west.png")),
            eastWest,
            ResourceLoader.loadResource(getTownResourcePath("road_north_south_east.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_north_south_west.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_north_east_west.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_south_east_west.png")),
            ResourceLoader.loadResource(getTownResourcePath("road_all.png")));
        return Collections.singletonList(render);
    }

    @Override
    public MazeRenderSettings createSettings() {
        return new MazeRenderSettings(100, 50);
    }
}
