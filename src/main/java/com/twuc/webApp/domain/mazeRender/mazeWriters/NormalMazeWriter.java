package com.twuc.webApp.domain.mazeRender.mazeWriters;

import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public abstract class NormalMazeWriter extends MazeWriter {
    protected NormalMazeWriter(MazeComponentFactory factory) {
        super(factory);
    }

    private int calculateDimension(int length) {
        return length * settings.getCellSize() + settings.getMargin() * 2;
    }

    private int translateCoordWithMargin(int original) {
        return original + settings.getMargin();
    }

    @Override
    public void render(Grid grid, OutputStream stream) throws IOException {
        RenderGrid renderGrid = new RenderGrid(grid);
        BufferedImage image = createImage(renderGrid);
        Graphics2D graphics = image.createGraphics();
        renderBackground(image.getWidth(), image.getHeight(), graphics);
        renderMaze(renderGrid, graphics);
        ImageIO.write(image, "png", stream);
    }

    private BufferedImage createImage(RenderGrid renderGrid) {
        int width = calculateDimension(renderGrid.getColumnCount());
        int height = calculateDimension(renderGrid.getRowCount());
        return new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
    }

    private void renderMaze(RenderGrid renderGrid, Graphics2D graphics) {
        for (RenderCell cell : renderGrid.getCells()) {
            Rectangle cellArea = calculateCellArea(cell);
            renderGround(graphics, cell, cellArea);
            renderWall(graphics, cell, cellArea);
        }
    }

    private void renderWall(Graphics2D graphics, RenderCell cell, Rectangle cellArea) {
        renderCell(graphics, cell, cellArea, RenderType.WALL, wallRenders);
    }

    private void renderGround(Graphics2D graphics, RenderCell cell, Rectangle cellArea) {
        renderCell(graphics, cell, cellArea, RenderType.GROUND, groundRenders);
    }

    private void renderCell(
        Graphics2D context,
        RenderCell cell,
        Rectangle cellArea,
        RenderType renderType,
        List<CellRender> renders) {
        if (cell.getRenderType() != renderType) { return; }
        for (CellRender render : renders) {
            render.render(context, cellArea, cell);
        }
    }

    private Rectangle calculateCellArea(RenderCell cell) {
        return new Rectangle(
                    translateCoordWithMargin(cell.getColumn() * settings.getCellSize()),
                    translateCoordWithMargin(cell.getRow() * settings.getCellSize()),
                    settings.getCellSize(),
                    settings.getCellSize());
    }

    private void renderBackground(int width, int height, Graphics2D graphics) {
        for (AreaRender backgroundRender : backgroundRenders) {
            Rectangle fullArea = new Rectangle(0, 0, width, height);
            backgroundRender.render(graphics, fullArea);
        }
    }
}

