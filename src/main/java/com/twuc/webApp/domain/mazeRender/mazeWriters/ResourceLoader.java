package com.twuc.webApp.domain.mazeRender.mazeWriters;

import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

class ResourceLoader {
    public static BufferedImage loadResource(String path) {
        try {
            return ImageIO.read(new ClassPathResource(path).getURL());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
