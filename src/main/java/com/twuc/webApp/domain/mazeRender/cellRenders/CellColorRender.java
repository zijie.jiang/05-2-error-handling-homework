package com.twuc.webApp.domain.mazeRender.cellRenders;

import com.twuc.webApp.domain.mazeRender.CellRender;
import com.twuc.webApp.domain.mazeRender.RenderCell;

import java.awt.*;

/**
 * 该渲染器使用纯色对迷宫节点进行渲染。
 */
public class CellColorRender implements CellRender {
    private final Color color;

    /**
     * 创建一个 {@link CellColorRender} 实例。
     *
     * @param color 用于渲染的颜色。
     */
    public CellColorRender(Color color) {
        this.color = color;
    }

    @Override
    public void render(Graphics context, Rectangle cellArea, RenderCell cell) {
        context.setColor(color);
        context.fillRect(cellArea.x, cellArea.y, cellArea.width, cellArea.height);
    }
}
